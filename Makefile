SRCS_S = server.c
OBJS_S = $(SRCS_S:.c=.o)
SRCS_C = client.c
OBJS_C = $(SRCS_C:.c=.o)
CC = cc
AR = ar
C_FLAGS = -Wall -Wextra -Werror# -O3 -g -fsanitize=address
LD_FLAGS := -L./libft -lft
SERVER = server
CLIENT = client
LD_FLAGS_OBJ = 
.PHONY: clean

all: $(SERVER) $(CLIENT)


clean:
	cd libft && make fclean
	rm -rf $(OBJS_S) $(OBJS_C) 

fclean: clean
	rm -rf $(SERVER) $(CLIENT)

re: fclean all


%.o: %.c server.h
	$(CC) $(C_FLAGS) -c -o $@ $< $(LD_FLAGS_OBJ)

$(SERVER):  libft/libft.a $(OBJS_S)
	$(CC) $(C_FLAGS) -o  $@ $(OBJS_S)  $(LD_FLAGS) 

$(CLIENT):  libft/libft.a $(OBJS_C) 
	$(CC) $(C_FLAGS) -o  $@ $(OBJS_C)  $(LD_FLAGS) 

libft/libft.a:
	cd libft && make fclean all


debug: C_FLAGS+= -g
debug: LD_FLAGS+= -fsanitize=address
debug: LD_FLAGS_OBJ+= -fsanitize=address
debug:  libft/libft.a $(OBJS_S) $(OBJS_C)
debug:  $(SERVER)
debug:  $(CLIENT)


/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/06 20:24:20 by amajid            #+#    #+#             */
/*   Updated: 2024/01/07 16:35:34 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "libft/libft.h"

int	send_char_to_server(unsigned char c, int server_pid)
{
	unsigned char	bit;

	bit = 0b10000000;
	while (bit)
	{
		if (bit & c)
		{
			if (kill(server_pid, SIGUSR1) == -1)
				return (0);
		}
		else
		{
			if (kill(server_pid, SIGUSR2) == -1)
				return (0);
		}
		bit >>= 1;
		usleep(100);
	}
	return (1);
}

int	main(int arg_num, char **args)
{
	unsigned int	i;

	if (arg_num < 3)
		exit(0);
	if (ft_atoi(args[1]) == -1)
		exit(0);
	usleep(500);
	i = 0;
	while (args[2][i])
	{
		if (!send_char_to_server(args[2][i], ft_atoi(args[1])))
		{
			ft_putendl_fd("client: ERROR", 1);
		}
		i++;
	}
}

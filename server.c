/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/06 18:52:19 by amajid            #+#    #+#             */
/*   Updated: 2024/01/07 16:37:59 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "server.h"
#include "libft/libft.h"

t_data	g_data = {0};

void	sigusr1(int t)
{
	unsigned char	bit;

	(void)t;
	bit = 0b10000000;
	g_data.c |= bit >> g_data.index;
	g_data.index++;
}

void	sigusr2(int t)
{
	(void)t;
	g_data.index++;
}

void	loop(char *pid_str, char *c_str)
{
	free(c_str);
	free(pid_str);
	while (1)
	{
		if (g_data.index >= 8)
		{
			ft_putchar_fd(g_data.c, 1);
			g_data.index = 0;
			g_data.c = 0;
		}
	}
}

int	main(void)
{
	char	*str;
	char	*client_str;
	char	*pid_str;
	char	*c_str;
	long	size;

	signal(SIGUSR1, sigusr1);
	signal(SIGUSR2, sigusr2);
	client_str = "./client";
	str = "abdelilah majid\n";
	pid_str = ft_itoa(getpid());
	size = ft_strlen(client_str) + 1
		+ ft_strlen(pid_str) + 2 + ft_strlen(str) + 3 + 1 + 10;
	c_str = malloc(size);
	ft_strlcpy(c_str, client_str, size);
	ft_strlcat(c_str, " ", size);
	ft_strlcat(c_str, pid_str, size);
	ft_strlcat(c_str, " '", size);
	ft_strlcat(c_str, str, size);
	ft_strlcat(c_str, "' &", size);
	ft_putstr_fd("pid = ", 1);
	ft_putendl_fd(pid_str, 1);
	system(c_str);
	loop(pid_str, c_str);
}
